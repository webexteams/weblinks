const jwt = require('jsonwebtoken');
const rp = require('request-promise');
const utils = require('./utils.js');

const jwt_iss = process.env.JWT_ISS;
const jwt_key = Buffer.from(process.env.JWT_KEY, 'base64');

function replySucessfulTokenCreation(sub, name, res, dbRecord) {
    const tokenJson = {
        iss: jwt_iss,
        sub: sub,
        name: name,
        exp: Math.floor(Date.now() / 1000) + (60 * 60) // TODO: set a better value
    };

    const tokenJwt = jwt.sign(tokenJson, jwt_key);
    res.send({userId: dbRecord.user_id, isPersisted:dbRecord.is_persisted, token: tokenJwt});
}

async function createToken(req, res) {
    const dbRecord = await utils.getWeblinkEntry(req.body.linkId);

    if (dbRecord == null) {
        res.status(404).send("Link Id is not present");
        return;
    }

    console.log(dbRecord.guest_id);
    // Persisted session
    if (!!dbRecord.guest_id) {
        if (req.body.password == dbRecord.password) {
            replySucessfulTokenCreation(dbRecord.guest_id, dbRecord.guest_name, res, dbRecord);
        } else {
            res.status(401).send("Unauthorized");
        }
    }  // Non Persisted session
    else {
        // create random guest_id
        const randomCreatedGuestId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        await utils.addTempGuestIdToWebLinkEntries(dbRecord, randomCreatedGuestId);
        replySucessfulTokenCreation(randomCreatedGuestId, dbRecord.guest_name, res, dbRecord);
    }

}

async function persistSession(req, res) {
    const dbRecord = await utils.getWeblinkEntry(req.body.linkId);

    if (dbRecord.link_id == null) {
        res.status(404).send("Link Id is not present");
    }
    if (req.body.password == null) {
        res.status(403).send("Password is not present");
    } else {
        // copy guest_id to temp_guest_id
        const guestId = dbRecord.temp_guest_id;
        await utils.persistSession(dbRecord, guestId, req.body.password, true);
        res.status(200).send("Session is saved");
    }
}

module.exports = {
    createToken: createToken,
    persistSession: persistSession
};

