const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const nocache = require('nocache');
const cors = require('cors');
const bot = require('./bot.js');
const api = require('./api.js');

const app = express();

app.use(bodyParser.json({ strict: false }));
app.use(nocache());
app.use(cors());

app.post('/v1/bot', bot.eventCallback);
app.post('/v1/token', api.createToken);
app.put('/v1/session', api.persistSession);

module.exports.handler = serverless(app);
