const jwt = require('jsonwebtoken');
const utils = require('./utils.js');

const bot_id = process.env.BOT_ID;
const bot_token = process.env.BOT_TOKEN;

const domain_name = "https://webex.direct/p/";
const domain_sep  = "#";
const help_string = `Try typing one of the following commands:\n
**/invite Guest Name** - Create a weblink for the provided guestName\n
**/list** - List all existing weblinks for your user\n
**/delete** {link} - Delete the provided link`;
const create_success_string = "Successfully created weblink for ";
const delete_success_string = "Successfully deleted weblink for ";

const invalid_command_string = "Sorry, I didn't understand your message! Type **/help** to see what I can do";
const no_links_string = "You currently have no weblinks! Use **/invite Guest Name** to create a weblink";
const no_guestName_string = "No guestName provided! Use **/invite Guest Name** to create a weblink";
const invalid_delete_string = "Invalid link provided! Use **/delete {link}** to delete a weblink";
const link_not_found_string = "This link doesn't exist!";

const create_failed_string = "Failed to create weblink";
const delete_failed_string = "Failed to delete weblink";

async function eventCallback(req, res) {

  const { id, personId } = req.body.data;
  
  if (personId == bot_id) {
    res.status(200).send({message: 'Message from self ignored'});
    return;
  }  

  utils.getMessage(id, 
    (text) => {
      handleCommand(text, personId, res);
    },
    () => {
        res.status(502).send({message: 'Failed to fetch the message'});
    }
  );
}

async function handleCommand(text, personId, res) {
  var input = text.split(" ");
  var command = input[0];
  console.log(command);
  if (command == "/help") {
    postReply(help_string, personId, res);
  } else if (command == "/list") {
    listWeblinks(personId, res);
  } else if (command == "/invite" || command == "/create") {
    createWeblink(input, personId, res);
  } else if (command == "/delete") {
     deleteWeblink(input, personId, res);
  } else {
    postReply(invalid_command_string, personId, res);
  }
}

async function listWeblinks(personId, res) {
  const dbRecords = await utils.getEntryByUserId(personId);
  if (dbRecords == null || dbRecords.length == 0) {
    postReply(no_links_string, personId, res);
  } else {
    var weblinksList = "You've created the following links:\n";
    dbRecords.forEach(function(item) {
      weblinksList += "- " + item.guest_name + " - " + domain_name + domain_sep + item.link_id + "\n";
    });
    postReply(weblinksList, personId, res);
  }
}

async function createWeblink(input, personId, res) {
  if (input.length == 1) {
    postReply(no_guestName_string, personId, res);
  } else {
    var guestName = input[1];
    for (i = 2; i < input.length; i++) {
      guestName += " " + input[i]; //TODO: Should we put a max on how many words in a guestName? Also, input validation? 
    }
    var linkId = utils.createRandomLinkId();
    try {
      await utils.insertEntryInTable(personId, linkId, guestName);
      postReply(create_success_string + guestName + " - " + domain_name + domain_sep + linkId, personId, res);
    } catch (e) {
      postReply(create_failed_string, personId, res);
    }
  }
}

async function deleteWeblink(input, personId, res) {
  if (input.length != 2) {
    postReply(invalid_delete_string, personId, res);
  } else {
    var link = input[1]; //TODO: Better input validation
    var link_split = link.split(domain_sep);
    if (link_split.length != 2) {
      postReply(invalid_delete_string, personId, res);
    } 
    else {
      const dbRecord = await utils.getWeblinkEntry(link_split[1]);
      if (dbRecord == null || dbRecord.user_id!=personId) {
        postReply(link_not_found_string, personId, res)
      } 
      else {
        try {
            await utils.deleteEntryInTable(dbRecord.link_id);
            postReply(delete_success_string + dbRecord.guest_name, personId, res);
        } catch (e) {
          postReply(delete_failed_string, personId, res);
        }
      }
    }
  }
}

function postReply(text, personId, res) {
  utils.postMessage(personId, text, null, 
    () => {
      res.send({message: 'Ok'});
    }, 
    ()=> {
      res.status(502).send({message: 'Failed to send the reply'});
    }
  );
}

module.exports = {
  eventCallback: eventCallback
};
