const awsSdk = require('aws-sdk');
const spark = require('ciscospark/env');
const https = require('https');

const dynamoDb = new awsSdk.DynamoDB.DocumentClient();
const bot_token = process.env.BOT_TOKEN;

//TODO: Implement DB access. Currently contains sample code only.

function getCurDate() {
    var d = new Date();
    d.setHours(0, 0, 0, 0);
    return d.toJSON();
}

async function getWeblinkEntry(linkId) {
    try {
        var dbRecord = await dynamoDb.get({TableName: 'weblinks_entries', Key: {link_id: linkId}}).promise();
        return dbRecord.Item;
    } catch (e) {
        return null;
    }
}

async function addTempGuestIdToWebLinkEntries(dbRecord, randomCreatedGuestId) {

    const tableName = "weblinks_entries";
    const item = {
        link_id: String(dbRecord.link_id),
        guest_name: String(dbRecord.guest_name),
        user_id: String(dbRecord.user_id),
        temp_guest_id: String(randomCreatedGuestId)
    }
    const params = {
        TableName: tableName,
        Item: item
    };
    await dynamoDb.put(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            console.log("Success in updating ", data);
        }
    });
}

async function persistSession(dbRecord, guestId, password, isPersisted) {

    const tableName = "weblinks_entries";
    const item = {
        link_id: String(dbRecord.link_id),
        guest_name: String(dbRecord.guest_name),
        password: String(password),
        guest_id: String(guestId),
        user_id: String(dbRecord.user_id),
        temp_guest_id: String(guestId),
        is_persisted: Boolean(isPersisted)
    }
    const params = {
        TableName: tableName,
        Item: item
    };
    await dynamoDb.put(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            console.log("Success in updating ", data);
        }
    });
}

async function getEntryByUserId(userId) {
    try {
        var params = {
            TableName: 'weblinks_entries',
            IndexName: 'user_id-guest_name-index',
            KeyConditionExpression: "user_id = :user_id",
            ExpressionAttributeValues: {
                ":user_id": userId
            }
        };

        var data = await dynamoDb.query(params, function (err, data) {
            if (err) {
                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            } else {
                console.log("Query succeeded.");
            }
        }).promise();

        return data.Items;
    } catch (e) {
        return null;
    }
}

async function insertEntryInTable(userId, linkId, guestName) {
    const tableName = "weblinks_entries";
    const item = {
        link_id: linkId,
        guest_name: guestName,
        user_id: userId,
    }
    const params = {
        TableName: tableName,
        Item: item
    };
    await dynamoDb.put(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            console.log("Success in creating ", data);
        }
    });
}

async function deleteEntryInTable(linkId) {
    const tableName = "weblinks_entries";
    const params = {
        TableName: tableName,
        Key: {
            link_id: linkId
        }
    };
    await dynamoDb.delete(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            console.log("Success in deleting ", data);
        }
    });
}

async function setRoomId(day, value) {
    return await dynamoDb.put({TableName: 'cl-management-bot', Item: {day: day, roomId: value}}).promise();
}

async function getOrCreateRoom() {
    var curDay = getCurDate();
    var roomId = await getRoomId(curDay);
    if (!roomId) {
        var roomRecord = await spark.rooms.create({title: 'Demo Space (' + curDay + ')'});
        await setRoomId(curDay, roomRecord.id);
        await logEvent("New space " + roomRecord.id + " created for " + curDay);
        return roomRecord.id;
    } else {
        return roomId;
    }
}

async function get(req, res) {
    const roomId = await getOrCreateRoom();
    res.json({
        version: '2019-01-13 001',
        roomId: roomId
    });
};

async function addMember(req, res) {
    const {userId, userEmail} = req.body;
    const roomId = await getOrCreateRoom();

    if (!userId && !userEmail) {
        res.status(400).json({error: 'Provide one of the userId or userEmail (not both)'});
    } else {
        try {
            await spark.memberships.create({
                roomId: roomId,
                personEmail: userEmail,
                personId: userId
            });

            res.status(204).end();
        } catch (e) {
            if (e.name == 'Conflict') {
                res.status(409).end();
            } else {
                console.error("Error", JSON.stringify(e));
                res.status(502).end(JSON.stringify(e));
            }
        }
    }
};

function postMessage(personId, markdown, imageUrl, success, error) {
    var message = {
        "toPersonId": personId,
        "markdown": markdown,
    };

    if (imageUrl) {
        message.files = [imageUrl];
    }

    console.log('Sending the message', message);

    var options = {
        hostname: 'api.ciscospark.com',
        port: 443,
        path: '/v1/messages',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + bot_token
        }
    };

    var req = https.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (body) {
            success(body);
        });
    });
    req.on('error', function (e) {
        error(e.message);
    });

    req.write(JSON.stringify(message));
    req.end();
}

function getMessage(messageId, success, error) {
    var options = {
        hostname: 'api.ciscospark.com',
        port: 443,
        path: '/v1/messages/' + messageId,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + bot_token
        }
    };

    var req = https.request(options, (res) => {
        res.on('data', (d) => {
            success(JSON.parse(d).text);
        });
    });
    req.end();

    req.on('error', (e) => {
        console.error(e.message);
        error(e);
    });
}

function createRandomLinkId() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

module.exports = {
    get: get,
    addMember: addMember,
    getOrCreateRoom: getOrCreateRoom,
    getWeblinkEntry: getWeblinkEntry,
    getMessage: getMessage,
    postMessage: postMessage,
    addTempGuestIdToWebLinkEntries: addTempGuestIdToWebLinkEntries,
    persistSession: persistSession,
    getEntryByUserId: getEntryByUserId,
    createRandomLinkId: createRandomLinkId,
    insertEntryInTable: insertEntryInTable,
    deleteEntryInTable: deleteEntryInTable
};
