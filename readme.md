# WebLinks

WebLinks bridges the gap between users of Webex Teams and users without an account.

## Running locally

Install the tools and dependencies:
```sh
npm install -g serverless
npm install
```

Set AWS credentials:
```sh
export AWS_ACCESS_KEY_ID=...
export AWS_SECRET_ACCESS_KEY=...
```

Run & test locally:
```sh
serverless offline
curl -X POST http://localhost:3000/v1/token
```
